class AuthsController < ApplicationController
    def index
        @Auths = Auth.all
        render json: @Auths
    end

    def show
        @Auth = Auth.find(params[:id])
        render json: @Auth
    end

    def create
        @Auth = Auth.create(
            menu_name:params[:menu_name],
            restaurant_name:params[:restaurant_name],
            menu_description: params[:menu_description]
        )
        render json: @Auth
    end

    def update
        @Auth = Auth.find(params[:id])
        @Auth.update(
            menu_name:params[:menu_name],
            restaurant_name:params[:restaurant_name],
            menu_description: params[:menu_description]
        )
        render json: @Auth
    end

    def destroy
        @Auths = Auth.all
        @Auth = Auth.find(params[:id])
        @Auth.destroy
        render json: @Auths
    end

end
